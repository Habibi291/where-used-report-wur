﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WuR.Reports;

namespace WuR
{
    public partial class frmReport : Form
    {
        public frmReport()
        {
            InitializeComponent();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            rptMaterialWhereUsed rptMaterialWhereUsed = new rptMaterialWhereUsed();
            ReportPrintTool  reportReportTool = new ReportPrintTool(rptMaterialWhereUsed);
            reportReportTool.ShowPreview();
        }
    }
}
